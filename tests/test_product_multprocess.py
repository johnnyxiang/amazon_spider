from amazon_spider.spiders.product_multprocess import ProductSpiderMultipleProcess

if __name__ == '__main__':
    asins = ['0000013714',
             '0001061240',
             '0001381245',
             '0001711105',
             '0001846590',
             '0002006790',
             '0002117649',
             '0002119285',
             '0002151235',
             '0002157292',
             '0002170051',
             '0002171422',
             '0002177056',
             '000217801X',
             '0002190915',
             '0002231182',
             '0002235595',
             '0002236788',
             '0002239256',
             '0002254042'
             ]

    driver_path = '../driver/chromedriver_mac'
    spider = ProductSpiderMultipleProcess(asins=asins, process_num=2, callback=print, driver_path=driver_path)
    spider.start_requests()
