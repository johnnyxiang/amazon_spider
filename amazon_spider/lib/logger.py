import logging
import os

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
logging.basicConfig(format='%(name)s - %(levelname)s - %(message)s')


def get_logger(name=None):
    if name is None:
        name = __name__

    return logging.getLogger(name)
