import traceback
from multiprocessing import Process, Queue, current_process

import amazon_spider

from amazon_spider.lib.utils import get_browser
from amazon_spider.lib.logger import get_logger
from amazon_spider.spiders.product_spider import ProductSpider


class ProductSpiderMultipleProcess(object):
    logger = get_logger(__name__)

    def __init__(self, asins, process_num=5, callback=None, country='US', driver_path=None):
        self.process_num = process_num
        self.country = country
        self.asins = asins
        self.callback = callback
        amazon_spider.driver_path = driver_path

    def worker(self, work_queue, browser, callback=None):
        try:
            while True:
                if work_queue.empty():
                    print("Done %s" % current_process().name)
                    break

                asin = work_queue.get()

                try:
                    ProductSpider(asin=asin, browser=browser, country=self.country, callback=callback).start_requests()
                except Exception as e:
                    print(traceback.format_exc())
                    self.logger.error("%s failed  with: %s" % (current_process().name, e))
        except:
            print(traceback.format_exc())
        finally:
            pass

    def start_requests(self):

        work_queue = Queue()
        processes = []

        for asin in self.asins:
            work_queue.put(asin)

        for w in range(self.process_num):
            browser = get_browser(profile=None, disable_js=True, disable_image=True, headless=True)
            p = Process(target=self.worker, args=(work_queue, browser, self.callback))
            p.start()
            processes.append([p, browser])

        for p in processes:
            try:
                p[0].join()
            except (KeyboardInterrupt, SystemExit):
                print('Exiting...Please wait!')
                p[0].terminate()
                p[0].join()
