from amazon_spider.lib.utils import get_browser, get_page_content_from_url, get_product_url
from amazon_spider.items import ProductItem
from amazon_spider.lib.logger import get_logger


class ProductSpider:
    logger = get_logger(__name__)

    def __init__(self, asin, country='US', browser=None, callback=None):
        self.asin = asin
        self.country = country
        self.browser = browser if browser is not None else self.get_browser()
        self.callback = callback

    @staticmethod
    def get_browser():
        return get_browser(profile=None)

    def start_requests(self):
        self.process(get_product_url(self.asin))

    def process(self, url_link):
        response = get_page_content_from_url(url_link, browser=self.browser)
        self.parse(response)

    def parse(self, response):
        product_item = ProductItem(asin=self.asin, html=response)
        if self.callback is not None:
            self.callback(product_item)
        else:
            return product_item


if __name__ == '__main__':
    asin = 'B00H5PJ0HW'
    ProductSpider(asin=asin, country='US', callback=print).start_requests()
