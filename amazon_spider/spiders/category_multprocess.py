import traceback
from multiprocessing import Process, Queue, current_process

import amazon_spider
from amazon_spider.lib.utils import read_csv_file
from amazon_spider.lib.logger import get_logger
from amazon_spider.spiders.category_spider import CategorySpider


class CategorySpiderMultipleProcess(object):
    url_file = None
    process_num = 5
    work_queue = Queue()
    done_queue = Queue()
    processes = set()

    logger = get_logger(__name__)

    def __init__(self, url_file, process_num=5, callback=None, driver_path=None):
        self.url_file = url_file
        self.process_num = process_num
        self.prepare_work_queue()
        self.callback = callback
        amazon_spider.driver_path = driver_path

    def prepare_work_queue(self):
        urls = read_csv_file(self.url_file)
        for link in urls:
            self.work_queue.put(link)

        self.process_num = min(self.process_num, len(urls))

    def worker(self):
        try:
            while not self.work_queue.empty():
                url = self.work_queue.get()
                try:
                    CategorySpider(url_link=url, callback=self.callback).start_requests()
                except Exception as e:
                    print(traceback.format_exc())
                    self.logger.error("%s failed  with: %s" % (current_process().name, e))
        except:
            print(traceback.format_exc())
        finally:
            pass

    def start_requests(self):

        for w in range(self.process_num):
            p = Process(target=self.worker)
            p.start()
            self.processes.add(p)

        for p in self.processes:
            p.join()
